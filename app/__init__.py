import os
from flask import Flask, render_template
from flask_sqlalchemy import SQLAlchemy
from config import config
from flask_restful import Api

flask_env = os.getenv('FLASK_ENV') or 'development'
loaded_configuration = config[flask_env]

app = Flask(__name__)
app.config.from_object(loaded_configuration)
loaded_configuration.init_app(app)

db = SQLAlchemy()
db.init_app(app)
api = Api(app, prefix='/api/v1', catch_all_404s=True)
