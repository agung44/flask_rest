from datetime import datetime
from .. import db
from .users_groups import association_table

class Group(db.Model):
    __tablename__ = 'groups';

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String, nullable=False, unique=True, index=True)
    created_at = db.Column(db.DateTime, default=datetime.utcnow, nullable=False)
    users = db.relationship('User',
            secondary=association_table,
            backref=db.backref('users', lazy='dynamic'),
            cascade='delete')

    def __init__(self, name):
        self.name = name
