from .. import db
from .users_groups import association_table

class User(db.Model):
    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String, nullable=False, unique=True, index=True)
    name = db.Column(db.String, nullable=False)
    groups = db.relationship('Group',
            secondary=association_table,
            backref=db.backref('groups', lazy='dynamic'),
            cascade='delete')


    def __init__(self, email, name):
        self.email = email
        self.name = name
