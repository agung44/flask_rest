from .. import db

association_table = db.Table('users_groups',
            db.Column('user_id', db.Integer, db.ForeignKey('users.id')),
            db.Column('group_id', db.Integer, db.ForeignKey('groups.id'))
        )
