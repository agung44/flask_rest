from flask_restful import fields, reqparse
from flask import jsonify
from . import BaseResource
from .. import api, db
from ..models.user import User
from ..models.group import Group

group_field = {
        'id': fields.Integer,
        'name': fields.String,
        'created_at': fields.DateTime(dt_format='rfc822')
        }

class GroupsByUserResource(BaseResource):
    def get(self, user_id):
        "GET: list all groups of a particular user"

        user = User.query.get_or_404(user_id)
        groups = user.groups

        return self.buildMarshal(groups, group_field, envelope='groups')


api.add_resource(GroupsByUserResource, '/user/<int:user_id>/groups')


