from sqlalchemy import func
from flask import jsonify
from flask_restful import fields, reqparse
from . import BaseResource
from .. import api, db
from ..models.user import User
from ..models.group import Group

user_field = {
        'id': fields.Integer,
        'name': fields.String,
        'email': fields.String
        }

class UsersResource(BaseResource):
    def get(self):
        "GET: listing all users ordered by name asc"

        users = User.query.order_by('name asc').all()
        return self.buildMarshal(users, user_field, envelope='users')


    def post(self):
        "POST: add new user"

        params = self.params()
        user = User(name=params['name'], email=params['email'])
        db.session.add(user)
        db.session.commit()
        return self.buildMarshal(user, user_field, envelope='user')

    def params(self):
        parser = reqparse.RequestParser()
        parser.add_argument('name', type=str, required=True)
        parser.add_argument('email', type=str, required=True)
        return parser.parse_args()


class UsersWithGroupCountResource(BaseResource):
    def get(self):
        """
        GET: List of users and a number of groups
        that users belong to - sorted by the number of
        groups in ascending order
        """

        user_field['group_count'] = fields.Integer
        users = db.session.query(User, func.count().label('group_count')).join(User.groups).group_by(User).order_by('group_count ASC').all()
        def injector(result):
            user = result[0]
            user.group_count = result[1]
            return user

        users = list(map(injector, users))
        return self.buildMarshal(users, user_field, envelope='users')

class UsersWithGroupNameResource(BaseResource):
    def get(self):
        """
        GET: List all users and the names of the groups
        that user belongs to - users
        are sorted by name in descending order
        """

        user_field['group_names'] = fields.String
        users = db.session.query(User, func.group_concat(Group.name).label('group_names')).join(User.groups).group_by(User).order_by('users.name DESC').all()
        def injector(result):
            user = result[0]
            user.group_names = result[1]
            return user

        users = list(map(injector, users))
        return self.buildMarshal(users, user_field, envelope='users')



api.add_resource(UsersResource, '/users')
api.add_resource(UsersWithGroupCountResource, '/users/group_count')
api.add_resource(UsersWithGroupNameResource, '/users/group_names')
