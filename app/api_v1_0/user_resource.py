from flask_restful import fields, reqparse
from flask import jsonify
from . import BaseResource
from .. import api, db
from ..models.user import User

user_field = {
        'id': fields.Integer,
        'name': fields.String,
        'email': fields.String
        }

class UserResource(BaseResource):
    def get(self, user_id):
        "GET: retrieve user by id"

        user = User.query.get_or_404(user_id)
        return self.buildMarshal(user, user_field, envelope='user')

    def put(self, user_id):
        "PUT: update user by id"

        params = self.params()
        user = User.query.get_or_404(user_id)
        user.email = params['email']
        user.name = params['name']
        db.session.add(user)
        db.session.commit()
        return self.buildMarshal(user, user_field, envelope='user')

    def delete(self, user_id):
        "DELETE: remove user by id"

        user = User.query.get_or_404(user_id)
        db.session.delete(user)
        db.session.commit()
        return jsonify(message='Succefully delete a user') 

    def params(self):
        parser = reqparse.RequestParser()
        parser.add_argument('name', type=str, required=True)
        parser.add_argument('email', type=str, required=True)
        return parser.parse_args()

api.add_resource(UserResource, '/user/<int:user_id>')


