from flask_restful import fields, reqparse
from flask import jsonify
from . import BaseResource
from .. import api, db
from ..models.user import User
from ..models.group import Group

class UserByGroupResource(BaseResource):
    def delete(self, group_id, user_id):
        "DELETE: removing user from group"

        group = Group.query.get_or_404(group_id)
        user = User.query.get_or_404(user_id)

        if group in user.groups:
            user.groups.remove(group)
            db.session.add(user)
            db.session.commit()

            msg = 'Succefully remove user with id: %s from group with id: %s' % (user.id, group.id)
            return jsonify(message=msg)
        else:
            msg = 'Failed to remove user with id: %s from group with id: %s' % (user.id, group.id)
            return ({'message': msg}, 400)

api.add_resource(UserByGroupResource, '/group/<int:group_id>/user/<int:user_id>')


