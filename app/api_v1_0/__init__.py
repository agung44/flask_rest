from flask_restful import Resource, reqparse, marshal
from flask import jsonify

class BaseResource(Resource):
    def args(self):
        parser = reqparse.RequestParser()
        parser.add_argument('pretty', type=bool, help='cannot convert to pretty mode')
        return parser.parse_args()

    def is_pretty(self):
        return self.args()['pretty'] is True

    def buildMarshal(self, obj, fields, envelope=None):
        marshaled = marshal(obj, fields, envelope=envelope)
        if self.is_pretty():
            return jsonify(marshaled)
        else:
            return marshaled


