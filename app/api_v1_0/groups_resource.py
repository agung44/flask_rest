from sqlalchemy import func
from flask_restful import fields, reqparse
from flask import jsonify
from . import BaseResource
from .. import api, db
from ..models.group import Group

group_field = {
        'id': fields.Integer,
        'name': fields.String,
        'created_at': fields.DateTime(dt_format='rfc822')
        }

class GroupsResource(BaseResource):
    def get(self):
        "GET: listing all groups ordered by name asc"
        groups = Group.query.order_by("name asc").all()
        return self.buildMarshal(groups, group_field, envelope='groups')

    def post(self):
        "POST: add new group"

        params = self.params()
        group = Group(name=params['name'])
        db.session.add(group)
        db.session.commit()
        return self.buildMarshal(group, group_field, envelope='group')

    def params(self):
        parser = reqparse.RequestParser()
        parser.add_argument('name', type=str, required=True)
        return parser.parse_args()

class GroupsWithUserCountResource(BaseResource):
    def get(self):
        """
        GET: List of groups and a number of users 
        belonging to each group
        sorted by the number of users in descending order
        """
        group_field['user_count'] = fields.Integer
        groups = db.session.query(Group, func.count().label('user_count')).join(Group.users).group_by(Group).order_by('user_count desc').all()
        def injector(result):
            group = result[0]
            group.user_count = result[1]
            return group

        groups = list(map(injector, groups))
        return self.buildMarshal(groups, group_field, envelope='groups')

api.add_resource(GroupsResource, '/groups')
api.add_resource(GroupsWithUserCountResource, '/groups/user_count')
