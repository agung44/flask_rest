from flask_restful import fields, reqparse
from flask import jsonify
from . import BaseResource
from .. import api, db
from ..models.group import Group

group_field = {
        'id': fields.Integer,
        'name': fields.String,
        'created_at': fields.DateTime(dt_format='rfc822')
        }

class GroupResource(BaseResource):
    def put(self, group_id):
        "PUT: update group by id"

        params = self.params()
        group = Group.query.get_or_404(group_id)
        group.name = params['name']
        db.session.add(group)
        db.session.commit()
        return self.buildMarshal(group, group_field, envelope='group')

    def delete(self, group_id):
        "DELETE: remove group by id"

        group = Group.query.get_or_404(group_id)
        db.session.delete(group)
        db.session.commit()
        return jsonify(message='Succefully delete a group') 


    def params(self):
        parser = reqparse.RequestParser()
        parser.add_argument('name', type=str)
        return parser.parse_args()

api.add_resource(GroupResource, '/group/<int:group_id>')
