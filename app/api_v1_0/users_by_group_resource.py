from flask_restful import fields, reqparse
from flask import jsonify
from . import BaseResource
from .. import api, db
from ..models.user import User
from ..models.group import Group

user_field = {
        'id': fields.Integer,
        'name': fields.String,
        'email': fields.String
        }

class UsersByGroupResource(BaseResource):
    def get(self, group_id):
        "GET: list all of users of a particular group"

        group = Group.query.get_or_404(group_id)
        users = group.users

        return self.buildMarshal(users, user_field, envelope='users')



    def post(self, group_id):
        "POST: add existing user into group"

        params = self.params()
        group = Group.query.get_or_404(group_id)
        user = User.query.get_or_404(params['user_id'])

        if group not in user.groups:
            user.groups.append(group)
            db.session.add(user)
            db.session.commit()

            msg = 'Succefully add user with id: %s to group with id: %s' % (user.id, group.id)
            return jsonify(message=msg)
        else:
            msg = 'Failed to add user with id: %s to group with id: %s' % (user.id, group.id)
            return ({'message': msg}, 400)


    def params(self):
        parser = reqparse.RequestParser()
        parser.add_argument('user_id', type=int, required=True)
        return parser.parse_args()

api.add_resource(UsersByGroupResource, '/group/<int:group_id>/users')


