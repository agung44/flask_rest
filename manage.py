#!/usr/bin/env/python
from app import app, db, api

from app.models.user import User
from app.models.group import Group

from app.api_v1_0.user_resource import UserResource
from app.api_v1_0.users_resource import UsersResource
from app.api_v1_0.group_resource import GroupResource
from app.api_v1_0.groups_resource import GroupsResource
from app.api_v1_0.users_by_group_resource import UsersByGroupResource
from app.api_v1_0.user_by_group_resource import UserByGroupResource
from app.api_v1_0.groups_by_user_resource import GroupsByUserResource

from flask_script import Manager, Shell
from flask_migrate import Migrate, MigrateCommand

from seed import perform_seed

manager = Manager(app)
migrate = Migrate(app, db)

def to_sql(query):
    print(str(query.statement.compile()))

def make_shell_context():
    return dict(app=app, db=db, User=User, Group=Group, to_sql=to_sql)

@manager.command
def routes():
    "Display available routes"
    # print(app.url_map)
    import urllib
    output = []
    for rule in app.url_map.iter_rules():

        options = {}
        for arg in rule.arguments:
            options[arg] = "[{0}]".format(arg)

        methods = ','.join(rule.methods)
        # url = url_for(rule.endpoint, **options)
        line = urllib.parse.unquote("{:50s} {:20s} {}".format(rule.endpoint, methods, rule))
        output.append(line)

    for line in sorted(output):
        print(line)

@manager.command
def seeds():
    "Populate database with sample data"
    perform_seed()

manager.add_command('shell', Shell(make_context=make_shell_context))
manager.add_command('db', MigrateCommand)

if __name__ == '__main__':
    manager.run()
