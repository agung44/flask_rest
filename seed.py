from faker import Faker
from app import db
from app.models.user import User
from app.models.group import Group
from random import randint

def generate_groups():
    faker = Faker()
    for x in range(10):
        name = "%s %d Group" % (faker.color_name(), x)
        g = Group(name=name)
        db.session.add(g)
    db.session.commit()

def generate_users():
    faker = Faker()
    for x in range(50):
        user = User(name = faker.name(), email = faker.email())
        rnd = randint(1, 7)
        user.groups = Group.query.order_by('random()').limit(rnd).all()
        db.session.add(user)
    db.session.commit()

def clear():
    Group.query.delete()
    User.query.delete()

    db.session.execute('delete from users_groups')
    db.session.commit()

def perform_seed():
    clear()
    generate_groups()
    generate_users()


