### How To USe

First initialize the virtualenv with available python binary:

```python
virtualenv -p /Users/[username]/.pyenv/versions/[python_version]/bin/python[version] venv
```
And load the python virtualenv to current shell.

```bash
source venv/bin/activate
```
Next, install dependencies using **pip** from **requirements.txt**

```bash
pip install -r requirements.txt
```
Also we need to run the migration, so the database and tables will be created.

```
python manage.py db upgrade
```

Then run the application with

```
python manage.py runserver -r
```

### Some extra information
- Seeds initial data

```
python manage.py seeds
```
- Display available routes

```bash
python manage.py routes
```
- Toggle response json with compact or pretty mode

```bash
# compact / default mode
curl http://localhost:5000/api/v1/users

# pretty mode
curl http://localhost:5000/api/v1/users?pretty=1
```

